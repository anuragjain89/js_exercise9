var RegistrationFormValidator = function (formToBeValidated) {
  this.formToBeValidated = formToBeValidated;
};

RegistrationFormValidator.prototype.validateNonEmpty = function (formToBeValidated) {
  var elementsToBeValidated = Array.prototype.slice.call(formToBeValidated.querySelectorAll('.non-empty input'));
  var flag = true;
  elementsToBeValidated.forEach(function (element) {
    if (element.value.length === 0) {
      flag = false;
      alert(element.name + " can't be empty.");
    }
  });
  return flag;
};

RegistrationFormValidator.prototype.validateMinLength50 = function (formToBeValidated) {
  var minLength = 50;
  var elementsToBeValidated = Array.prototype.slice.call(formToBeValidated.querySelectorAll('.min50'));
  var flag = true;
  elementsToBeValidated.forEach(function (element) {
    if (element.value.length < minLength) {
      flag = false;
      alert(element.name + " can't have a length less than " + minLength);
    }
  });
  return flag;
};

RegistrationFormValidator.prototype.URL_REGEX = /(?:ht|f)tps?\:\/\/\S+$/im;
RegistrationFormValidator.prototype.EMAIL_REGEX = /^\S+\@\S+\.\S+$/im;

RegistrationFormValidator.prototype.validateURL = function (formToBeValidated) {
  var flag = true;
  var elementsToBeValidated = Array.prototype.slice.call(formToBeValidated.querySelectorAll('.url input'));
  elementsToBeValidated.forEach(function (element) {
    if (!element.value.trim().match(RegistrationFormValidator.prototype.URL_REGEX)) {
      flag = false;
      alert('Please enter a valid url for ' + element.name);
    }
  });
  return flag;
};

RegistrationFormValidator.prototype.validateEmail = function (formToBeValidated) {
  var flag = true;
  var elementsToBeValidated = Array.prototype.slice.call(formToBeValidated.querySelectorAll('.email input'));
  elementsToBeValidated.forEach(function (element) {
    if (!element.value.trim().match(RegistrationFormValidator.prototype.EMAIL_REGEX)) {
      flag = false;
      alert('Please enter a valid email for ' + element.name);
    }
  });
  return flag;
};

RegistrationFormValidator.prototype.validateReceiveNotification = function (formToBeValidated) {
  var flag = true;
  var elementsToBeValidated = Array.prototype.slice.call(formToBeValidated.querySelectorAll('.notify input[type="checkbox"]'));
  elementsToBeValidated.forEach(function (element) {
    var message;
    if (element.checked === true) {
      message = 'Are you sure you want to receive notifications ?';
    } else {
      message = 'Are you sure you don\'t want to receive notifications ?';
    }
    flag = flag && confirm(message);
  });
  return flag;
};

RegistrationFormValidator.prototype.submitHandler = function (event) {
  var isValidNonEmpty = RegistrationFormValidator.prototype.validateNonEmpty(this);
  var isValidMinLength50 = RegistrationFormValidator.prototype.validateMinLength50(this);
  var isValidEmail = RegistrationFormValidator.prototype.validateEmail(this);
  var isValidURL = RegistrationFormValidator.prototype.validateURL(this);

  if (isValidNonEmpty && isValidMinLength50 && isValidEmail && isValidURL) {
    var isValidNotified = RegistrationFormValidator.prototype.validateReceiveNotification(this);
    if (!isValidNotified) {
      event.preventDefault();
    }
  } else {
    event.preventDefault();
  }
};

RegistrationFormValidator.prototype.initValidations = function () {
  this.formToBeValidated.addEventListener('submit', this.submitHandler);
};

var registrationFormWithValidations = new RegistrationFormValidator(document.forms[0]);
registrationFormWithValidations.initValidations();